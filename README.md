Тестовое задание
================

Простейший блог.


Порядок запуска:

    sudo apt-get install node
    sudo npm install -g bower
    git clone https://lapdev@bitbucket.org/lapdev/test_blog.git
    mkvirtualenv test_blog
    pip install -r requirements.txt
    ./manage.py syncdb
    ./manage.py createsuperuser
    ./manage.py migrate
    ./manage.py bower install
    ./manage.py runserver