from django.conf.urls import patterns, include, url
from .views import *


urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^post/(?P<pk>\d+)/$', BlogPostView.as_view(), name='blog_post'),
    url(r'^tag/(?P<pk>\d+)/$', TagView.as_view(), name='tag'),
)
