# coding: utf-8
from __future__ import unicode_literals
from django.contrib import admin
from .models import *


class CommentInline(admin.TabularInline):
    model = Comment


class MainPageOptionsAdmin(admin.ModelAdmin):
    class Media:
        js = 'jquery/dist/jquery.min.js', 'js/admin/hide_add_button.js',


class BlogPostAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', 'contents', 'tags')
        }),
        ('СЕО поля', {
            'fields': ('keywords', 'description')
        }),
    )
    list_filter = 'tags__name',
    list_display = '__str__', 'created_at'
    inlines = CommentInline,


class CommentAdmin(admin.ModelAdmin):
    list_display = 'post', 'username', 'created_at'


class TagAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name',)
        }),
        ('СЕО поля', {
            'fields': ('keywords', 'description')
        }),
    )


admin.site.register(MainPageOptions, MainPageOptionsAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(BlogPost, BlogPostAdmin)
admin.site.register(Comment, CommentAdmin)