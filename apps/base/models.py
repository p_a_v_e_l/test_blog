# coding: utf-8
from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.encoding import python_2_unicode_compatible


class DateTimeModelMixin(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Время последнего редактирования')


class SeoModelMixin(models.Model):
    class Meta:
        abstract = True

    keywords = models.TextField(verbose_name='Ключевики (мета-тег)', blank=True)
    description = models.TextField(verbose_name='Описание (мета-тег)', blank=True)


@python_2_unicode_compatible
class MainPageOptions(SeoModelMixin, DateTimeModelMixin, models.Model):
    class Meta:
        verbose_name = 'Настройки главной страницы'
        verbose_name_plural = 'Настройки главной страницы'

    def __str__(self):
        return 'Настройки главной страницы'


@python_2_unicode_compatible
class Tag(SeoModelMixin, DateTimeModelMixin, models.Model):
    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'
        ordering = 'created_at',

    name = models.CharField(max_length=255, verbose_name='Тег')

    def __str__(self):
        return 'Тег <{0}>'.format(self.name)

    def get_absolute_url(self):
        return reverse('tag', args=[str(self.id)])


@python_2_unicode_compatible
class BlogPost(SeoModelMixin, DateTimeModelMixin, models.Model):
    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
        ordering = 'created_at',

    title = models.CharField(max_length=255, verbose_name='Заголовок')
    contents = models.TextField(verbose_name='Содержимое')
    tags = models.ManyToManyField(Tag, verbose_name='Теги', blank=True)

    def __str__(self):
        return 'Пост <{0}>'.format(self.title)

    def get_absolute_url(self):
        return reverse('blog_post', args=[str(self.id)])


@python_2_unicode_compatible
class Comment(DateTimeModelMixin, models.Model):
    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        ordering = 'created_at',

    post = models.ForeignKey(BlogPost, verbose_name='Пост')
    contents = models.CharField(max_length=255, verbose_name='Текст комментария')
    username = models.CharField(max_length=50, verbose_name='Имя комментатора')

    def __str__(self):
        return 'Комментарий <{0}... от {1}>'.format(self.contents[:10], self.username)