# coding: utf-8
from __future__ import unicode_literals
from django import forms
from .models import Comment


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = 'username', 'contents'

    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = 'Имя'
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['contents'].label = 'Комментарий'
        self.fields['contents'].widget = forms.widgets.Textarea()
        self.fields['contents'].widget.attrs['class'] = 'form-control'

    def clean_username(self):
        username = self.cleaned_data['username']
        if len(username) < 2:
            raise forms.ValidationError('Убедитесь, что это значение содержит не менее 2 символов (сейчас {0}).'.format(len(username)))
        if ' ' in username:
            raise forms.ValidationError('Это значение не должно содержать пробелов')
        return username