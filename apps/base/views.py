from django.views.generic import ListView, DetailView
from .models import *
from .forms import CommentForm


class IndexView(ListView):
    queryset = BlogPost.objects.all().order_by('-created_at')
    paginate_by = 3
    template_name = 'index.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super(IndexView, self).get_context_data(*args, **kwargs)
        kwargs['main_page'] = MainPageOptions.objects.all()[0]
        return kwargs


class BlogPostView(DetailView):
    model = BlogPost
    template_name = 'blog_post.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super(BlogPostView, self).get_context_data(*args, **kwargs)
        kwargs['comments'] = Comment.objects.filter(post=self.object)
        if not 'form' in kwargs:
            kwargs['form'] = CommentForm()
        return kwargs

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        form = CommentForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.post = self.object
            obj.save()
            context = self.get_context_data(object=self.object)
        else:
            context = self.get_context_data(object=self.object, form=form)
        return self.render_to_response(context)


class TagView(DetailView):
    model = Tag
    template_name = 'tag.html'

    def get_context_data(self, *args, **kwargs):
        # delegating listview-y functionality to a "subaltern" view for better code reuse
        class AuxTagView(ListView):
            paginate_by = 3
            queryset = BlogPost.objects.filter(tags=self.object).order_by('title')


        aux_view = AuxTagView()
        aux_view.request = self.request
        aux_view.args = self.args
        aux_view.kwargs = self.kwargs

        kwargs['object_list'] = aux_view.get_queryset()
        page_size = aux_view.get_paginate_by(kwargs['object_list'])
        if page_size:
            paginator, page, queryset, is_paginated = aux_view.paginate_queryset(kwargs['object_list'], page_size)
            kwargs.update({
                'paginator': paginator,
                'page_obj': page,
                'is_paginated': is_paginated,
                'object_list': queryset
            })
        else:
            kwargs.update({
                'paginator': None,
                'page_obj': None,
                'is_paginated': False
            })
        return kwargs