$(document).ready(function() {
    // no way to add/delete an object (if only maliciously)
    $('a.addlink').css('display', 'none');
    $('div.actions').css('display', 'none');
    $('p.deletelink-box').css('display', 'none');
    $('input[name="_addanother"]').css('display', 'none');
});