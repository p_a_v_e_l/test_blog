from .models import Tag


def top_tags(request):
    return {
        'top_tags': Tag.objects.all().order_by('name')[:10]
    }