from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()
admin.site.index_template = 'admin/custom_index.html'
admin.site.app_index_template = 'admin/custom_app_index.html'

urlpatterns = patterns('',
    url(r'^', include('base.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
